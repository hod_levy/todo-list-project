export{};
const express = require('éxpress');
const {Todo, validateDate} = require('../models/todo');
const router = express.Router();

router.use(express.json());

router.delete('/:id', async (req: any, res: any) => {
    let todo = await Todo.findByIdAndUpdate(req.params.id, {
        $unset: {finishDate: 1}
    });
    if(!todo) return res.status(404).send('ToDo with the given id was not found');
    
    todo = await todo.save();
    res.send(todo);
});

router.put('/:id', async (req: any, res: any) => {
    const { error } = validateDate(req.body);
    if(error) return res.status(404).send('Validation schema failed, missing valid finish Date');

    let todo = await Todo.findByIdAndUpdate(req.params.id, {
        finishDate: req.body.finishDate
    });
    if(!todo) return res.status(404).send('ToDo with the given id was not found');

    todo = await todo.save();
    res.send(todo);
});

module.exports = router;