export{};
const {TodoList, validateTodoList} = require('../models/todo-list');
const express = require('express');
const router = express.Router();

router.use(express.json());

router.get('/', async (res: any) => {
    const todoLists = await TodoList.find();
    res.send(todoLists);
});

router.get('/:id', async (req: any, res: any) => {
    const todoList = await TodoList.findById(req.params.id);
    if(!todoList) return res.status(404).send('Todo list with the given id was not found.');

    res.send(todoList);
});

router.post('/', async (req: any, res: any) => {
    const { error } = validateTodoList(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let todoList = new TodoList({
        name: req.body.name
    });

    todoList = await todoList.save();
    res.send(todoList);
});

router.delete('/:id', async (req: any, res: any) => {
    const todoList = await TodoList.findByIdAndRemove(req.params.id);

    if(!todoList) return res.status(404).send('The ToDo with the given id was not found');

    res.send(todoList);
});

module.exports = router;