export{};
const todoDate = require('../routes/todo-date');
const todoToggle = require('../routes/todo-toggle');
const {Todo, validateTodo} = require('../models/todo');
const express = require('express');
const router = express.Router();
const app = express();

router.use(express.json());
app.use('/api/todo/date', todoDate);
app.use('/api/todo/toggle', todoToggle);

router.get('/', async (res: any) => {
    const todos = await Todo.find();
    res.send(todos);
});

router.get('/:id', async (req: any, res: any) => {
    const todo = await Todo.findById(req.params.id);
    res.send(todo);
});

router.post('/', async (req: any, res: any) => {
    const { error } = validateTodo(req.body);

    if(error) return res.status(400).send(error.details[0].message);

    let todo = new Todo({
        listId: req.body.listId, //need to handle with non-exist listId
        task: req.body.task,
        finishDate: req.body.finishDate
    });
    todo = await todo.save();

    res.send(todo);
});

router.delete('/:id', async (req: any, res: any) => {
    const todo = await Todo.findByIdAndRemove(req.params.id);

    if(!todo) return res.status(404).send('The ToDo with the given id was not found');

    res.send(todo);
});

router.put('/:id', async (req: any, res: any) => {
    const {error} = validateTodo(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const todo = await Todo.findByIdAndUpdate(req.params.id, {
        task: req.body.task,
        finishDate: req.body.finishDate
    })
});

module.exports = router;
