export{};
const todo = require('../routes/todo');
const todoList = require('../routes/todo-list');
const mongoose = require('mongoose');
const express = require('express');
const app = express();
const {config, messages} = require('./config');

app.use(express.json());
app.use('/api/todo', todo);
app.use('/api/todo-list', todoList);

mongoose.connect(config.mongodbUrl, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log(messages.dbSuccesfulConnection))
    .catch((err: any) => console.log(messages.dbFailConnection, err));

const port = process.env.PORT || config.serverDefaultPort;
app.listen(port, () => console.log(`Listening on port ${port}...`));