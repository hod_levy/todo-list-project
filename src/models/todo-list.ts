export{};
const mongoose = require('mongoose');
const Joi = require('joi');

const TodoList = mongoose.model('todoList', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 15
    }
}));

function validateTodoList(todo: typeof TodoList){
    const schema = {
        name: Joi.string().min(5).max(15).required()
    };

    return Joi.validate(todo, schema);
}

exports.TodoList = TodoList;
exports.validateTodoList = validateTodoList;