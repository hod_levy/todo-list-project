export{};
const mongoose = require('mongoose');
const Joi = require('joi');
const {TodoList} = require('../models/todo-list');

const Todo = mongoose.model('todo', new mongoose.Schema({
    listId: {
        type: String,
        required: true,
        validate:{
            isAsync: true,
            validator: async function(v: String, callback: any){
                const todoList = await TodoList.findById(v);
                callback(todoList);
            }
        }
    },
    task: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 25
    },
    finishDate: {
        type: Date,
        required: false,
        validate: {
            validator: function(v: Date) {
                return v && v.getDate() <= Date.now();
            },
            message: 'Future date is not acceptable.'
        }
    }
}));

function validateTodo(todo: typeof Todo){
    const schema = {
        listId: Joi.string().required(),
        task: Joi.string().min(3).max(25).required(),
        finishDate: Joi.date().max(Date.now())
    };

    return Joi.validate(todo, schema);
}

function validateDate(date: any){
    const schema = {
        finishDate: Joi.date().max(Date.now()).required()
    }

    return Joi.validate(date, schema);
}

exports.Todo = Todo;
exports.validateTodo = validateTodo;
exports.validateDate = validateDate;