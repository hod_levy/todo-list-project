const config = {
    serverDefaultPort: 4000,
    serverAddress: 'localhost',
    mongodbUrl: 'mongodb://localhost/todo-list' 
};

const messages = {
    dbSuccesfulConnection: 'Connected to MongoDB...',
    dbFailConnection: 'Could not connect to MongoDB...'
};

exports.config = config;
exports.messages = messages;